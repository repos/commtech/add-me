/* eslint-env node */
/* eslint-disable no-console */

const fs = require( 'fs' );
const { mwn } = require( 'mwn' );
const { execSync } = require( 'child_process' );

// JSON file with your username and bot password. Set up a bot password at [[Special:BotPasswords]]
const credentials = require( '../credentials.json' );

// Relative path to the source file.
const SCRIPT_SOURCE = '../dist/AddMe.js';
// Name of the on-wiki script page name
const SCRIPT_PAGE = 'MediaWiki:Gadget-addMe-v2.js';

/* eslint-disable-next-line new-cap */
const bot = new mwn( {
	apiUrl: 'https://meta.wikipedia.org/w/api.php',
	username: credentials.username,
	password: credentials.password
} );

// Version info for edit summary.
const sha = execSync( 'git rev-parse --short HEAD' ).toString();
const summary = process.argv[ 2 ] || execSync( 'git log -1 --pretty=%B' ).toString().trim();
const version = require( '../package.json' ).version;

bot.login().then( () => {
	const outjs = fs.readFileSync( __dirname + '/' + SCRIPT_SOURCE ).toString();

	bot.save( SCRIPT_PAGE, outjs, `v${version} at ${sha.trim()}: ${summary}` ).then( () => {
		console.log( `Successfully saved dist/AddMe.js to ${SCRIPT_PAGE}` );
	}, ( e ) => {
		console.log( `Failed to edit ${SCRIPT_PAGE}: ${e}` );
	} );
} );
