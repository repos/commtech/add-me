import Dialog from './Dialog';

// <nowiki>
/**
 * @class
 * @property {jQuery} $content
 * @property {mw.Api} api
 * @property {string} project Which key to use when loading the configuration and translations.
 * @property {Array.<Object>} config Config fetched from AddMe.configPage.
 * @property {Dialog} dialog
 */
class AddMe {

	static configPage = 'MediaWiki:Gadget-addMe-config.json';
	static messagesPage = 'MediaWiki:Gadget-addMe-messages';

	/**
	 * @constructor
	 * @param {jQuery} $content The content area as provided by the mediawiki.content hook.
	 */
	constructor( $content ) {
		if ( !$content.find( '.addme-button' ) ) {
			return;
		}

		this.$content = $content;
		this.api = new mw.Api();
		this.project = null;
		this.config = {
			// Which page to post the comment to. If null or blank, it uses the current page.
			page: '',
			// The anchor of the section of the page to post the comment to.
			'section-anchor': null,
			// Maximum level of section to process; used to help prevent putting comments in the
			//   wrong place if there are multiple sections with the same title.
			'max-section-level': null,
			// Wikitext to prepend before the comment, such as a {{support}} template.
			'prepend-content': '* ',
			// Whether to skip checking if the user already commented on the page.
			'skip-dup-check': false,
			// Whether to restrict use of AddMe to logged-in users.
			'require-login': true,
			// Regular expression used to removed unwanted content from the comment
			//   (such as a {{support}} template).
			'remove-content-regex': null,
			// Edit summary to use.
			'edit-summary': '',
			// Where to link to when there are unrecoverable errors.
			'error-report-page': 'Meta talk:AddMe'
		};
		this.dialog = null;
	}

	/**
	 * Add the click listener.
	 */
	addListeners() {
		this.$content.find( '.addme-button' ).on( 'click', ( e ) => {
			this.setInternalMessages();

			this.project = e.target.dataset.addmeProject;
			if ( !this.project ) {
				return this.showAlert(
					this.log( 'addme-error-button-project' )
				);
			}

			this.config.page = e.target.dataset.addmePage || this.config.page ||
				mw.config.get( 'wgPageName' );
			// Use underscores since we'll be comparing against wgPageName again.
			this.config.page = this.config.page.replace( /\s/g, '_' );

			// To prevent abuse, ensure the target page is a subpage of the current page.
			// Presumably the subpage is transcluded.
			if (
				e.target.dataset.addmePage &&
				!this.config.page.includes( mw.config.get( 'wgPageName' ) + '/' ) &&
				this.config.page !== mw.config.get( 'wgPageName' )
			) {
				return this.showAlert(
					this.log( 'addme-error-subpage' )
				);
			}

			this.fetchConfig()
				.then( this.showDialog.bind( this ) )
				.fail( this.showAlert.bind( this ) );

			// Lazy-load postEdit module (only shown on desktop).
			if ( !OO.ui.isMobile() ) {
				mw.loader.using( 'mediawiki.action.view.postEdit' );
			}
		} );
	}

	/**
	 * Fetch the configuration and messages, then set the appropriate class properties.
	 *
	 * @return {jQuery.Deferred}
	 */
	fetchConfig() {
		const dfd = $.Deferred();

		if ( mw.messages.get( 'addme-submit' ) && this.config.page ) {
			// Everything is already loaded.
			return dfd.resolve();
		}

		const langPageEn = `${AddMe.messagesPage}/en`,
			langPageLocal = `${AddMe.messagesPage}/${mw.config.get( 'wgUserLanguage' )}`,
			titles = [
				AddMe.configPage,
				// Always fetch English so that we have fallbacks for each message.
				// The payloads are small.
				langPageEn
			];
		if ( mw.config.get( 'wgUserLanguage' ) !== 'en' ) {
			// Fetch the translation in the user's language, if not English.
			titles.push( langPageLocal );
		}

		this.api.get( {
			action: 'query',
			prop: 'revisions',
			titles,
			rvprop: 'content',
			rvslots: 'main',
			format: 'json',
			formatversion: 2
		} ).then( ( resp ) => {
			let messagesLocal = {},
				messagesEn = {};

			resp.query.pages.forEach( ( page ) => {
				if ( page.missing ) {
					switch ( page.title ) {
						case AddMe.configPage:
							dfd.reject(
								this.log( `Missing configuration page [[${AddMe.configPage}]]` )
							);
							break;
						case langPageEn:
							dfd.reject(
								this.log( `Missing base language page [[${langPageEn}]]` )
							);
							break;
						case langPageLocal:
							// Don't reject in this case and instead log a warning to the console.
							this.log( `Localization for '${mw.config.get( 'wgUserLanguage' )}' missing at [[${langPageLocal}]]`, 'warn' );
							break;
					}
				} else {
					const pageObj = page.revisions[ 0 ].slots.main;
					const parsedContent = this.parseJSON( page.title, pageObj.content );

					if ( pageObj.contentmodel === 'json' ) {
						// We know it's the config page.
						this.config = Object.assign(
							this.config,
							parsedContent.default || {},
							parsedContent[ this.project ]
						);
					} else if ( page.title === langPageLocal && mw.config.get( 'wgUserLanguage' ) !== 'en' ) {
						messagesLocal = Object.assign(
							parsedContent.messages.default || {},
							parsedContent.messages[ this.project ]
						);
					} else {
						messagesEn = Object.assign(
							parsedContent.messages.default || {},
							parsedContent.messages[ this.project ]
						);
					}
				}
			} );

			// Prefix all messages with 'addme-' so there's no clashing with MW messages.
			const messages = Object.assign( {}, messagesEn, messagesLocal );
			Object.keys( messages ).forEach( ( key ) => {
				delete Object.assign(
					messages,
					{ [ `addme-${key}` ]: messages[ key ] }
				)[ key ];
			} );

			mw.messages.set( messages );

			dfd.resolve();
		} );

		return dfd;
	}

	/**
	 * Add raw English error messages to mw.messages. These are fatal errors
	 * not intended to be seen by users (i.e. AddMe was misconfigured), so
	 * they are not setup for translation.
	 */
	setInternalMessages() {
		if ( mw.messages.exists( 'addme-error-alert' ) ) {
			// Already loaded;
			return;
		}

		// Add in configuration error messages that don't need to be translatable.
		mw.messages.set( {
			'addme-error-alert': 'There was an error with the AddMe gadget: <i>$1</i>\n' +
				`Please report this issue at [[${this.config[ 'error-report-page' ]}]].`,
			'addme-error-parse': 'Unable to parse the configuration page [[$1]]. ' +
				'There may have been a recent change that contains invalid JSON.',
			'addme-error-button-project': "Button is missing the 'data-addme-project' attribute.",
			'addme-error-subpage': "The 'data-addme-page' attribute is not a subpage of the current page.",
			'addme-error-section-missing': 'The "$1" section is missing from [[$2]]. ' +
				'Please correct this error or report this issue at [[$3]].'
		} );
	}

	/**
	 * The content model of the messages page is wikitext so that it can be used with
	 * Extension:Translate. Consequently, it's easy to break things. This just does
	 * a try/catch and indicates the likely culprit to the user.
	 *
	 * @param {string} title
	 * @param {string} content
	 * @return {Object}
	 */
	parseJSON( title, content ) {
		try {
			// Remove HTML that's added for untranslated messages.
			content = content.replace( /<span lang="en".*?>(.*?)<\/span>/g, '$1' );
			return JSON.parse( content );
		} catch {
			this.showAlert( 'addme-error-parse', title );
		}
	}

	/**
	 * Show the submission dialog.
	 */
	showDialog() {
		if ( this.config[ 'require-login' ] && !mw.config.get( 'wgUserName' ) ) {
			OO.ui.alert( mw.msg( 'addme-error-login' ) );
			return;
		}

		// Get the OOUI window manager, which opens and closes the dialog.
		const windowManager = OO.ui.getWindowManager();

		// Instantiate and show the dialog.
		if ( !this.dialog ) {
			this.dialog = new Dialog( this );
			windowManager.addWindows( [ this.dialog ] );
		}

		// Open the dialog and add focus to the textarea.
		windowManager.openWindow( this.dialog )
			.opened.then( () => this.dialog.textarea.focus() );
	}

	/**
	 * Submit the comment and watch status to the page.
	 *
	 * @param {string} comment
	 * @param {boolean} watch
	 * @return {jQuery.Deferred}
	 */
	submit( comment, watch ) {
		const dfd = $.Deferred();

		// Cleanup the comment.
		comment = comment.replace( '~~~~', '' );
		if ( this.config[ 'remove-content-regex' ] ) {
			comment = comment.replace( new RegExp( this.config[ 'remove-content-regex' ] ), '' );
		}
		if ( /^(?:\*|#)/.test( this.config[ 'prepend-content' ] ) ) {
			// Replace newlines with <p> tags, otherwise it will break the <ul> list.
			comment = comment.replace( /\n+/g, '<p>' );
		}
		comment = `\n${this.config[ 'prepend-content' ]}${comment.trim().replace( /<p>$/, '' )} ~~~~`;

		this.findSection()
			.then( this.updateSection.bind( this, comment, watch ) )
			.catch( ( message ) => {
				if ( message.constructor.name === 'OoUiError' ) {
					message = message.message;
				}
				dfd.reject( new OO.ui.Error( message || mw.msg( 'addme-error-save' ) ) );
			} )
			.then( dfd.resolve );

		return dfd;
	}

	/**
	 * Purge the contents of the page. This is necessary when the comment
	 * was added to a transcluded page.
	 *
	 * @return {jQuery.Deferred|jQuery.Promise}
	 */
	purgePage() {
		if ( this.config.page === mw.config.get( 'wgPageName' ) ) {
			// We're on the same page as the comment is on, so no need to purge.
			return $.Deferred().resolve();
		}

		return this.api.post( {
			action: 'purge',
			titles: mw.config.get( 'wgPageName' )
		} );
	}

	/**
	 * Reload the content on the page with the newly added comment.
	 * Some of this was copied from Extension:DiscussionTools / controller.js
	 *
	 * @return {jQuery.Promise}
	 */
	reloadContent() {
		return this.api.get( {
			action: 'parse',
			// HACK: we need 'useskin' so that reply links show (T266195)
			useskin: mw.config.get( 'skin' ),
			mobileformat: OO.ui.isMobile(),
			uselang: mw.config.get( 'wgUserLanguage' ),
			prop: [ 'text', 'sections', 'revid', 'jsconfigvars' ],
			page: mw.config.get( 'wgPageName' ),
			// HACK: Always display reply links afterwards, ignoring preferences etc.
			dtenable: '1',
			formatversion: 2
		} ).then( ( data ) => {
			// Actually replace the content.
			this.$content.find( '.mw-parser-output' )
				.first()
				.replaceWith( data.parse.text );

			mw.config.set( data.parse.jsconfigvars );

			// Update revision ID for other gadgets that rely on it being accurate.
			mw.config.set( {
				wgCurRevisionId: data.parse.revid,
				wgRevisionId: data.parse.revid
			} );

			// eslint-disable-next-line no-jquery/no-global-selector
			$( '#t-permalink a, #coll-download-as-rl a' ).each( function () {
				const url = new URL( this.href );
				url.searchParams.set( 'oldid', data.parse.revid );
				$( this ).attr( 'href', url.toString() );
			} );

			mw.hook( 'wikipage.content' ).fire( this.$content );
			mw.hook( 'wikipage.tableOfContents' ).fire(
				data.parse.showtoc ? data.parse.sections : []
			);
			this.addListeners();

			if ( OO.ui.isMobile() ) {
				mw.notify( mw.msg( 'addme-feedback' ) );
			} else {
				// postEdit is currently desktop only
				mw.hook( 'postEdit' ).fire( {
					message: mw.msg( 'addme-feedback' )
				} );
			}
		} ).fail( () => {
			// Comment was saved, but reloading failed. Redirect user to the (sub)page instead.
			window.location = mw.util.getUrl( this.config.page );
		} );
	}

	/**
	 * Add the comment to the given section.
	 *
	 * @param {string} comment
	 * @param {boolean} watch
	 * @param {Object} section
	 * @param {string} timestamp
	 * @return {jQuery.Promise}
	 */
	updateSection( comment, watch, section, timestamp ) {
		return this.api.postWithEditToken( {
			action: 'edit',
			title: this.config.page,
			section: section.index,
			summary: this.config[ 'edit-summary' ],
			starttimestamp: timestamp,
			nocreate: true,
			watchlist: watch ? 'watch' : 'nochange',
			appendtext: comment
		} );
	}

	/**
	 * Fetch section headers from this.config.page, and locate the one we're trying to edit.
	 * If no section header constraint is configured, we assume the final section.
	 * If a section header is configured but not found, an error is shown to the user.
	 *
	 * @return {jQuery.Deferred<Object,string>} Deferred promise resolving with section object
	 *   and the current server timestamp.
	 */
	findSection() {
		const dfd = $.Deferred();

		this.api.get( {
			format: 'json',
			formatversion: 2,
			action: 'parse',
			prop: 'sections|wikitext',
			page: this.config.page,
			curtimestamp: true,
			// FIXME: may not work if the source language page is not '/en'?
			uselang: 'en'
		} ).done( ( result ) => {
			const sections = result.parse.sections;
			// Locate the section we're trying to edit.
			let section;
			if ( this.config[ 'section-anchor' ] ) {
				// eslint-disable-next-line no-shadow
				section = sections.find( ( section ) => {
					const withinMaxLevel = this.config[ 'max-section-level' ] ?
						section.toclevel <= this.config[ 'max-section-level' ] :
						true;
					return section.anchor === this.config[ 'section-anchor' ] && withinMaxLevel;
				} );

				if ( !section ) {
					dfd.reject(
						new OO.ui.Error(
							mw.message(
								'addme-error-section-missing',
								this.config[ 'section-anchor' ],
								this.config.page,
								this.config[ 'error-report-page' ]
							).parseDom(),
							{ recoverable: false }
						)
					);
				}
			} else {
				// If no section was configured, fallback to using the last section.
				section = sections[ sections.length - 1 ];
			}

			if (
				!this.config[ 'skip-dup-check' ] &&
				!this.dialog.alreadyVoted &&
				this.checkForDupComment( result.parse.wikitext )
			) {
				this.dialog.alreadyVoted = true;
				dfd.reject(
					new OO.ui.Error(
						mw.msg( 'addme-error-dup-comment' ),
						{ recoverable: true, warning: true }
					)
				);
			}

			dfd.resolve( section, result.curtimestamp );
		} ).catch( ( response ) => {
			let logMsg = `There was an error when fetching section titles for [[${this.config.page}]]`,
				msg = 'error-save',
				recoverable = true;

			if ( response === 'missingtitle' ) {
				logMsg = `The page [[${this.config.page}]] is missing.`;
				msg = 'error-fatal';
				recoverable = false;
			}

			this.log( logMsg );
			dfd.reject(
				// Messages that can be used here:
				// * addme-error-save
				// * addme-error-fatal
				new OO.ui.Error( mw.msg( `addme-${msg}`, this.config[ 'error-report-page' ] ), { recoverable } )
			);
		} );

		return dfd;
	}

	/**
	 * Test if the user has already commented in the given wikitext.
	 *
	 * @param {string} wikitext
	 * @return {boolean}
	 */
	checkForDupComment( wikitext ) {
		const regEscape = ( str ) => str.replace( /[-/\\^$*+?.()|[\]{}]/g, '\\$&' );
		const regexp = new RegExp(
			regEscape( this.config[ 'prepend-content' ] ) + '.*?' +
			regEscape( mw.config.get( 'wgUserName' ) ) +
			'.*?\\(UTC\\)'
		);
		return regexp.test( wikitext );
	}

	/**
	 * Show an error to the user using an OOUI alert dialog.
	 *
	 * @param {string} key Message key (i.e. from this.setInternalMessages) or plain text.
	 * @param {...string} params
	 */
	showAlert( key, ...params ) {
		let msg = key;
		if ( mw.messages.exists( key ) ) {
			// Messages that can be used here:
			// * addme-error-alert
			// * addme-error-button-project
			// * addme-error-parse
			// * addme-error-section-missing
			// * addme-error-subpage
			msg = mw.msg( key, ...params );
		}
		OO.ui.alert(
			mw.message( 'addme-error-alert', msg, ...params ).parseDom(),
			{ title: 'Something went wrong' }
		);
	}

	/**
	 * Log an error to the console.
	 *
	 * @param {string} key Message key.
	 * @param {string} level One of: 'info', 'warn', 'error' (default)
	 * @return {string} The given message key.
	 */
	log( key, level = 'error' ) {
		// Messages that can be used here:
		// * addme-error-button-project
		// * addme-error-fatal
		// * addme-error-save
		// * addme-error-subpage
		// eslint-disable-next-line no-console
		console[ level ]( `[AddMe] ${mw.messages.exists( key ) ? mw.msg( key ) : key}` );
		return key;
	}
}

/**
 * Entry point, called after the 'wikipage.content' hook is fired.
 *
 * @param {jQuery} $content
 */
function init( $content ) {
	Promise.all( [
		// Resource loader modules
		mw.loader.using( [ 'oojs-ui', 'mediawiki.util', 'mediawiki.api', 'mediawiki.jqueryMsg' ] ),
		// Page ready
		$.ready
	] ).then( () => {
		const addMe = new AddMe( $content );
		addMe.addListeners();

		// Remove listener so that AddMe is only instantiated once.
		mw.hook( 'wikipage.content' ).remove( init );
	} );
}

mw.hook( 'wikipage.content' ).add( init );
// </nowiki>
