/**
 * @class
 * @property {AddMe} addMe
 * @property {boolean} alreadyVoted
 */
class Dialog extends OO.ui.ProcessDialog {

	static name = 'addmeDialog';
	static size = 'medium';

	/**
	 * @param {AddMe} addMeContext
	 * @constructor
	 */
	constructor( addMeContext ) {
		super();
		this.addMe = addMeContext;
		this.alreadyVoted = false;

		// Messages that aren't available in the static context.
		Dialog.title = mw.msg( 'addme-title' );
		Dialog.actions = [
			{ action: 'submit', label: mw.msg( 'addme-submit' ), flags: [ 'primary', 'progressive' ] },
			{ label: mw.msg( 'addme-cancel' ), flags: 'safe' }
		];

		// Add properties needed by the ES5-based OOUI inheritance mechanism.
		// This roughly simulates OO.inheritClass()
		Dialog.parent = Dialog.super = OO.ui.ProcessDialog;
		OO.initClass( OO.ui.ProcessDialog );
		Dialog.static = Object.create( OO.ui.ProcessDialog.static );
		Object.keys( Dialog ).forEach( ( key ) => {
			Dialog.static[ key ] = Dialog[ key ];
		} );
	}

	/**
	 * @param {...*} args
	 * @override
	 */
	initialize( ...args ) {
		super.initialize( ...args );
		this.editFieldset = new OO.ui.FieldsetLayout();
		this.content = new OO.ui.PanelLayout( { padded: true, expanded: false } );
		this.content.$element.append( this.editFieldset.$element );
		this.textarea = new OO.ui.MultilineTextInputWidget( {
			placeholder: mw.msg( 'addme-placeholder-comment' )
		} );
		this.watchCheckbox = new OO.ui.CheckboxInputWidget( { selected: false } );
		const formElements = [
			new OO.ui.FieldLayout( this.textarea, {
				label: mw.msg( 'addme-description' ),
				align: 'top'
			} ),
			new OO.ui.FieldLayout( this.watchCheckbox, {
				label: mw.msg( 'addme-watch-page' ),
				align: 'inline'
			} )
		];
		this.editFieldset.addItems( formElements );

		const parsedBody = mw.message( 'addme-body' ).parseDom();
		this.content.$element.append(
			`<p>${parsedBody.html() || parsedBody.text()}</p>`
		);
		this.$body.append( this.content.$element );
	}

	/**
	 * @param {string} action
	 * @override
	 */
	getActionProcess( action ) {
		return super.getActionProcess( action )
			.next( () => {
				if ( action === 'submit' ) {
					return this.addMe.submit(
						this.textarea.getValue(),
						this.watchCheckbox.isSelected()
					);
				}

				return super.getActionProcess( action );
			} )
			.next( () => {
				if ( action === 'submit' ) {
					return this.addMe.purgePage()
						.then( this.addMe.reloadContent.bind( this.addMe ) )
						.then( () => {
							this.close( { action } );
						} );
				}

				return super.getActionProcess( action );
			} );
	}

	/**
	 * @param {Object} data
	 * @override
	 */
	getSetupProcess( data ) {
		return super.getSetupProcess( data )
			.next( () => {
				this.textarea.setValue( '' );
				this.alreadyVoted = false;
			} );
	}
}

module.exports = Dialog;
